pragma solidity ^0.4.8;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Estatechain.sol";

contract TestEstatechain {
  
  function testInitialBalanceUsingDeployedContract() {

    // Create new estatechain contract
    Estatechain estate = Estatechain(DeployedAddresses.Estatechain());

    // Set expected test result
    uint256 expected = 1000000;

    // Check test return is equal to expected result
    Assert.equal(estate.balanceOf(msg.sender), expected, "Owner should have 1000000 EstateCoin(s) initially");
  }

  function testInitialBalanceWithNewEstateCoin() {
    
    // Create new estatechain contract
    Estatechain estate = new Estatechain();

    // Set expected test result
    uint256 expected = 1000000;

    // Check test return is equal to expected result
    Assert.equal(estate.balanceOf(address(this)), expected, "Owner should have 1000000 EstateCoin(s) initially");

    //Kill the contract
    estate.kill();
  }

  function testWhitelistWithNewEstateCoin() {

    // Create new estatechain contract
    Estatechain estate = new Estatechain();

    //Create new whitelist
    address accountaddress = 0xa8af5430d3219a2beab33f60afe7eced667871c1;

    //Add to whitelist
    bool success = estate.addwhitelist(accountaddress);
    Assert.equal(success, true, "Add to whitelist");

    //Transfer amount
    uint transferamount = 5;
    uint expected = 1000000 - 5;
    
    //Transfer and test
    Assert.equal(estate.transfer(accountaddress, transferamount), true, "Transfer");
    Assert.equal(estate.balanceOf(accountaddress), transferamount, "To account should have 5 EstateCoin(s)");
    Assert.equal(estate.balanceOf(address(this)), expected, "Owner in whitelist should have 5 less EstateCoin(s)");

    //Kill the contract
    estate.kill();
  }

  function testDeactivateUserWhitelistWithNewEstateCoin() {

    // Create new estatechain contract
    Estatechain estate = new Estatechain();

    //Create new whitelist
    address accountaddress = 0xa8af5430d3219a2beab33f60afe7eced667871c1;

    //Add to whitelist
    bool success = estate.addwhitelist(accountaddress);
    Assert.equal(success, true, "Add  to whitelist");

    //Deactivate********************************************************
    success = estate.deactivatewhitelistuser(accountaddress);
    Assert.equal(success, true, "Deactivate from whitelist");

    //Transfer amount
    uint transferamount = 0;
    uint expected = 1000000;
    
    //Transfer and test
    Assert.equal(estate.transfer(accountaddress, transferamount), false, "Transfer");
    Assert.equal(estate.balanceOf(accountaddress), transferamount, "To account should have 0 EstateCoin(s)");
    Assert.equal(estate.balanceOf(address(this)), expected, "Owner in whitelist should have 5 less EstateCoin(s)");

    //Reactivate whitelist user
     success = estate.activatewhitelistuser(accountaddress);
    Assert.equal(success, true, "Activate Paul to whitelist");

    //Transfer amount
    transferamount = 5;
    expected = 1000000-5;
    
    //Transfer and test
    Assert.equal(estate.transfer(accountaddress, transferamount), true, "Transfer");
    Assert.equal(estate.balanceOf(accountaddress), transferamount, "To account should have 5 EstateCoin(s)");
    Assert.equal(estate.balanceOf(address(this)), expected, "Owner in whitelist should have 5 less EstateCoin(s)");

    //Kill the contract
    estate.kill();
  }

  function testAdminlistWithNewEstateCoin() {
    
    // Create new estatechain contract
    Estatechain estate = new Estatechain();

    //Create new whitelist
    address accountaddress = 0xa8af5430d3219a2beab33f60afe7eced667871c1;

    //Add to whitelist
    bool success = estate.addwhitelist(accountaddress);
    Assert.equal(success, true, "Add to whitelist");

    //Create new adminlist
    address adminaddress = 0xa8af5430d3219a2beab33f60afe7eced667871c1;
    bytes32 name = "KPMG";

    //Add to adminlist
    success = estate.addadminlist(adminaddress, name);
    Assert.equal(success, true, "Add KPMG to adminlist");

    //Verify adminlist
    success = estate.verifyadminlist(adminaddress);
    Assert.equal(success, true, "Verify KPMG is in adminlist");

    //Remove from admin list
    success = estate.removefromadminlist(adminaddress);
    Assert.equal(success, true, "Remove KPMG from adminlist");

    //Verify adminlist
    success = estate.verifyadminlist(adminaddress);
    Assert.equal(success, false, "Verify KPMG is in adminlist");

    //****Cannot add someone to the whitelist as KMPG until on live test network */
  }
}
