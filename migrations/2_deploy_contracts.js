var ConvertLib = artifacts.require("./ConvertLib.sol");
var Estatechain = artifacts.require("./Estatechain.sol");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, Estatechain);
  deployer.deploy(Estatechain);  
};

