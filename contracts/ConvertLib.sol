//This library is used to convert currency based on a rate
pragma solidity ^0.4.8;

library ConvertLib {

	// Convert currency based on rate
	function convert(uint amount, uint conversionRate) returns (uint convertedAmount)
	{
		// Return the converted value / amount
		return amount * conversionRate;
	}
}
