// This library is used to manage the master owner access and kill the contract
pragma solidity ^0.4.4;

import "../contracts/AdminLib.sol";

library MasterOwnerLib {

    // Only allow access to the master owner with this function
    function onlyByMasterOwner(address _sender, address _masterOwner) internal {
        require(_sender == _masterOwner);
    }

    // Only allow access to the master owner or an Admin with this function
    function onlyByMasterOnwerOrAdmin(address _masterOwner, AdminLib.Adminlist[] _adminlist, address _sender) internal {
        require(_sender == _masterOwner || AdminLib.verifyadminlist(_adminlist, _sender));
    }

    // Allow the current master Owner to change the master owner
    function changeMasterOwner(address _sender, address _masterOwner, address _newMasterOwner) internal returns (address newmasterOwner) {
        require(_newMasterOwner != 0x0);
        require(_sender == _masterOwner);
        return newmasterOwner;
    }

    // Allow the master owner to kill (a.k.a delete) this contract
    function kill(address _sender, address _masterOwner) internal {
        require(_sender == _masterOwner);
        selfdestruct(_masterOwner);
    }
}