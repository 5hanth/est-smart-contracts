/*
This Token Contract implements the standard token functionality and extends it for use by Estatechain
.*/
pragma solidity ^0.4.8;

import "../contracts/MasterOwnerLib.sol";
import "../contracts/AdminLib.sol";
import "../contracts/StandardToken.sol";

contract Estatechain is StandardToken {

    // Mapping
    mapping (address => bool) public whitelist;   //Map the whitelist flag to addresses

    // Local variables	
    string public name;                         // Contract / Company name
    uint8 public decimals;                      // How many decimals to show. ie. There could 1000 base units with 3 decimals. Meaning 0.980 SBX = 980 base units. It's like comparing 1 wei to 1 ether.
    string public symbol;                       // An identifier / symbol often used for trading
    string public version = "0.1";              // Human 0.1 standard. Just an arbitrary versioning scheme.
    address public masterOwner;                 // Hold the maser owner address
    AdminLib.Adminlist[] private adminlist;     // Admin list of those allowed to update the whitelist

    // Constructor
    function Estatechain() {                      
        masterOwner = msg.sender;                // Set Master Owner
        totalSupply = 1000000;                   // Set total supply
        balances[msg.sender] = 1000000;          // Give the creator all initial tokens
        name = "Estatechain";                    // Set the name for display purposes
        decimals = 3;                            // Set number of decimals
        symbol = "EST";                          // Set the symbol

         // Add master owner to the whitelist
        addwhitelist(msg.sender);
    }

/*******************************************Constant Get Functions*************************************/
    function getName() constant public returns (string _name) {
         return name;
    }

    function getSymbol() constant public returns (string _symbol) {
        return symbol;
    }

    function getVersion() constant public returns (string _version) {
        return version;
    }

    function getOwner() constant public returns (address _masterOwner) {
        return masterOwner;
    }

    function getTotalSupply() constant public returns (uint256 _totalSupply) {
        return totalSupply;
    }
/******************************************************************************************************/
/*******************************************Master Owner Calls*****************************************/
    // Change / Update the master owner of this contract
    function changeMasterOwner(address _newMasterOwner) public returns (bool success) {
        
        // Call the master ower library function to update the owner and return
        masterOwner = MasterOwnerLib.changeMasterOwner(msg.sender, masterOwner, _newMasterOwner);
        return true;
    }

    // Kill this contract
    function kill() public {

        // Call the master owner library function to kill this contract, note: only the master can kill this contract
        MasterOwnerLib.kill(msg.sender, masterOwner);
    }
/******************************************************************************************************/
/**************************************************Admin List******************************************/
	// Add to the adminlist - Multiple people can have different accounts (a.k.a addresses), but you cannot have multiple account addresses in the list
	function addadminlist(address _adminaddress, bytes32 _name) public returns (bool success) {	
        
        // Verifty this is the master owner calling
        MasterOwnerLib.onlyByMasterOwner(msg.sender, masterOwner);

        // Add to the admin list and return if successful
        return AdminLib.addadminlist(adminlist, _adminaddress, _name);
	}

    // Get the list length
	function getadminlistlength() constant public returns (uint256 length) {
		
        // Verifty this is the master owner calling
        MasterOwnerLib.onlyByMasterOwner(msg.sender, masterOwner);

        // Call and return the list length
        return AdminLib.getadminlistlength(adminlist);
	}

    // Get an index item address
	function getadminlistaddress(uint256 index) constant public returns (address adminaddress) {
		
        // Verifty this is the master owner calling
        MasterOwnerLib.onlyByMasterOwner(msg.sender, masterOwner);

        // Call and return the address
        return AdminLib.getadminlistaddress(adminlist, index);
	}

    // Get an index item name
	function getadminlistname(uint256 index) constant public returns (bytes32 name) {
        
        // Verifty this is the master owner calling
        MasterOwnerLib.onlyByMasterOwner(msg.sender, masterOwner);

        // Call and return the name
        return AdminLib.getadminlistname(adminlist, index);
	}

    // Remove an item from the list
	function removefromadminlist(address _adminaddress) public returns (bool success) {
		
        // Verifty this is the master owner calling
        MasterOwnerLib.onlyByMasterOwner(msg.sender, masterOwner);
        
        // Call remove item and return if successful
        return AdminLib.removefromadminlist(adminlist, _adminaddress);
	}

    // Verify an item is in the list
	function verifyadminlist(address _adminaddress) constant public returns (bool success) {
		
        // Verifty this is the master owner calling
        MasterOwnerLib.onlyByMasterOwner(msg.sender, masterOwner);
        
        // Call verify item and return if successful
        return AdminLib.verifyadminlist(adminlist, _adminaddress);
	}

/******************************************************************************************************/
/**********************************************White List**********************************************/
	// Add to the whitelist - Multiple people can have different accounts (a.k.a addresses), but you cannot have multiple account addresses in the list
	function addwhitelist(address _accountaddress) public returns (bool success) {	
				
        // Verifty this is either the master owner or an admin calling
        MasterOwnerLib.onlyByMasterOnwerOrAdmin(masterOwner, adminlist, msg.sender);

        // Set the whitelist flag
        whitelist[_accountaddress] = true;

        //Return true if successful
		return true;
	}

    // Get an index item
	function getwhitelist(address _accountaddress) constant public returns (bool active) {

        // Verifty this is either the master owner or an admin calling
        MasterOwnerLib.onlyByMasterOnwerOrAdmin(masterOwner, adminlist, msg.sender);

        // Get whitelist flag
        active = whitelist[_accountaddress];

        // Return the item
        return (active);
	}

    // Activate a item
	function activatewhitelistuser(address _accountaddress) public returns (bool success) {   
        
        // Verifty this is either the master owner or an admin calling
        MasterOwnerLib.onlyByMasterOnwerOrAdmin(masterOwner, adminlist, msg.sender);

        // Set this account to active (e.g. in whitelist)
        whitelist[_accountaddress] = true;

        //Return true if successful
        return true;
	}

    // Deactivate a item
	function deactivatewhitelistuser(address _accountaddress) public returns (bool success) {

        // Verifty this is either the master owner or an admin calling
        MasterOwnerLib.onlyByMasterOnwerOrAdmin(masterOwner, adminlist, msg.sender);

        // Set this account to inactive (e.g. not active whitelist member)
        whitelist[_accountaddress] = false;

        //Return true if successful
        return true;
	}

    // Verify that an item exist in the list
	function verifywhitelist(address _accountaddress1, address _accountaddress2) constant internal returns (bool success) {
        
        // Verify multipl address
        if (whitelist[_accountaddress1] == true && whitelist[_accountaddress2] == true) {
            return true;
        // Not found return false
        } else {return false;}      
	}
/******************************************************************************************************/
/*************************************Overwrite standardToken******************************************/
    // Overwrite the standard transfer function with whitelist controls
    function transfer(address _to, uint256 _value) public returns (bool success) {
        
        // Check whitelist access and return (needed for test script)
        if (verifywhitelist(msg.sender, _to) == false) {
            return false;
        }
        
        // Use require as standard condition and call the standard token transfer function
        require(verifywhitelist(msg.sender, _to) == true); 
        return standardTransfer(msg.sender, _to, _value);
    }

    // Overwrite the standard transfer function with payout whitelist controls
    function transferpayout(address _to, uint256 _value) private returns (bool success) {
        return standardTransfer(msg.sender, _to, _value);
    }

    // Overwrite the standard total supply function with master owner controls
	function updateTotalSupply(uint256 _value) public  returns (bool success) {
        
        // Verifty this is the master owner calling
        MasterOwnerLib.onlyByMasterOwner(msg.sender, masterOwner);

        // Call the standard total supply function and return if successful
        return standardUpdateTotalSupply(msg.sender, _value);
    }
/******************************************************************************************************/
}