pragma solidity ^0.4.4;

library AdminLib {

	// Adminlist data structure
	struct Adminlist { 
        address adminaddress;
        bytes32 name;
    }

	// Add to the adminlist - Multiple people can have different accounts (a.k.a addresses), but you cannot have multiple account addresses in the list
	function addadminlist(Adminlist[] storage _adminlist, address _adminaddress, bytes32 _name) internal returns (bool success) {	
               
        // Make sure that address isn't already in the adminlist
        if (verifyadminlist(_adminlist, _adminaddress) == true) {
            return false;
        }

		// Create the new adminlist item
		Adminlist memory newadminlist; 
        newadminlist.adminaddress = _adminaddress;
        newadminlist.name = _name;

        // Push the new item into the blockchain array		
		_adminlist.push(newadminlist);
		return true;
	}

    // Provide the array length
	function getadminlistlength(Adminlist[] _adminlist) constant internal returns (uint256 length) {

        // Return length
        return _adminlist.length;
	}

	// Get an admin address at a given index
	function getadminlistaddress(Adminlist[] _adminlist, uint256 index) constant internal returns (address adminaddress) {
        
        // Create variable from the array index
        adminaddress = _adminlist[index].adminaddress;

        // Return the admin
        return adminaddress;
	}

 	// Get an admin name at a given index
	function getadminlistname(Adminlist[] _adminlist, uint256 index) constant internal returns (bytes32 name) {
        
        // Create variable from the array index
        name = _adminlist[index].name;

        // Return the admin
        return name;
	}

    // Remove an item from the list
	function removefromadminlist(Adminlist[] storage _adminlist, address _adminaddress) internal returns (bool success) {
		       
        // Get the list length
        uint256 length = getadminlistlength(_adminlist);      

        // Search for the item
        for (uint i = 0; i < length; i++) {
            // When you find the account
            if (_adminlist[i].adminaddress == _adminaddress) {
                // If the list has just one item
                if (length == 1) {
                    // Remove the one item
                    delete _adminlist[length-1];

                    // Found and removed, return true
                    return true;
                }

                // Remove (to prevent black) and resize the array
                for (uint foundIndex = i; i<length-1; foundIndex++) {
                    _adminlist[foundIndex] = _adminlist[foundIndex+1];
                }
                    // Remove the last item
                    delete _adminlist[length-1];

                    // Found and removed, return true
                    return true;
            }
		}
        // Not found return false
        return false;
	}

    // Verify that an item exist in the list
	function verifyadminlist(Adminlist[] _adminlist, address _adminaddress) constant internal returns (bool success) {
		        
        // Get the list length
        uint256 length = getadminlistlength(_adminlist);

        // Search for the account
        for (uint i = 0; i < length; i++) {
            // If you find the item reture true
            if (_adminlist[i].adminaddress == _adminaddress) {
                return true;
            }
		}
        // Not found return false
        return false;
	}
}